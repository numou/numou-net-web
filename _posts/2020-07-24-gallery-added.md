---
layout: post
title: Gallery added!
categories: [Announcements]
tags: [gallery]
---

I added a proper [gallery](/gallery/) and removed the links to the Google Photos albums. I'm not really a fan of hosting my content on Google, and I plan on moving away from them
whenever I can. Adding my own gallery was a lot of work, but I figured enough out that I could scrounge something together. 

I ended up using [fancybox3](https://fancyapps.com/fancybox/3/) for the fancy gallery effects. It doesn't really work perfectly with the NSFW tagged images; 
they end up being put in as seperate elements and don't connect with the rest of the images in the slideshow. 

Oh well. It works enough for now!