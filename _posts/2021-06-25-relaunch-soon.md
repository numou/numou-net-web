---
layout: post
title: Relaunch soon!
categories: [Announcements]
date: 2021-06-25
---

Just a quick heads up - I'm relaunching this site soon. I'm thinking I'll be sticking with the whole static site generator thing; I like the control it gives me. Not sure if I'll be keeping the gallery long term - might move it to another site to keep it separate from the blog. On this site I want to focus on blogging and posting my stories - which I still haven't done yet!

Might come as soon as the next day or two. Enough putting it off, I need to just hurry up and get it done. 😋
