---
layout: page
title: About
---

### About this site

Welcome to the personal website for **Numou the Impfox**! Currently a work in progress. Right now I just have a gallery, 
where I will share my commissions and gift art. I have not drawn any of it; I am not an artist.

I also plan on uploading my stories that I've written on FurAffinity.

*Contact* page coming soon.